FROM alpine:3.17.3
ARG package
RUN apk add --no-cache ncurses-libs && \
    apk add --no-cache libstdc++
ADD $package /todo
CMD ["/todo/bin/todo", "foreground"]
