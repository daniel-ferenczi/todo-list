# todo
A multi-user todo-application, supporting concurrent users, and persistance of data.  
It supports the following operations:
  - Creation of todo lists for a given user
  - Addition of items to a todo list
  - Removal of items from a todo list
  - Listing the contents of a todo list

## Releases
Release tar.gz packages are generated with rebar3 in the project's CI. Please look for the latest releases in the [project release page](https://gitlab.com/daniel-ferenczi/todo-list/-/releases).

## Requirements
The application needs `ncurses-libs` and `libstdc++` to be installed on the target system.

## Usage
The server is registered globally, which allows connected nodes to act as clients without the need to run the server code themselves.

### Launching and connecting to the server node
  1. Start the application with the `todo` executable in the bin folder
  ```
  ./todo foreground
  ```
  2. Connect to the node from another shell session
  ```
  ./todo remote_console
  ```
### Server setup
  1. Once connected, initialize the database on the nodes you wish to run it on. As the data is persisted, this is only required for the first start
  ```
  todo:install([node()]).
  ```
  2. Start `mnesia` and the `todo` application
  ```
  application:start(mnesia), application:start(todo).
  ```

### Client setup
Clients only need to ensure a connection to a server node, for example with
```
net_kernel:connect_node('server@host').
```
### Managing todo lists
  The supported operations can be used with the following commands:
  ```
  todo_srv:create_todo("John Galt", "Monday").  
  todo_srv:add_item("John Galt", "Monday", "Build motor").  
  todo_srv:delete_item("John Galt", "Monday", "Build motor").  
  todo_srv:list_items("John Galt", "Monday").  
  ```
### Monitoring
The application provides monitoring metrics in the prometheus format, by courtesy of [Ilya Khaprov's prometheus library stack](https://github.com/deadtrickster/prometheus.erl).  
To access metrics, call `http://localhost:9000/metrics`

## Demo setup
### Docker
Prebuilt containers can be used for demo purposes and are available in the [container registry](https://gitlab.com/daniel-ferenczi/todo-list/container_registry). To use them run
```
docker run registry.gitlab.com/daniel-ferenczi/todo-list:1.0.0
docker exec -it <container id> /todo/bin/todo remote_console
```
And proceed as described in [server setup](#server-setup).

### Docker compose
A more complex demo setup based on docker compose is also avaiable. It includes two containers running `todo`, one has port 9000 exposed for prometheus scraping and has the intent to be used as a designated application and database server and the other as a client. `todo-server` mounts the `./data` folder from the host to persist data between docker compose sessions. The setup also includes `prometheus` and `grafana` containers with their scraping and datasource configuration set up.
After running `docker compose up`, a [server setup](#server-setup) is still needed however.

## Local tests and builds
To test locally, use
```
rebar3 ct
```
Builds can be performed with
```
rebar3 as prod release
```
