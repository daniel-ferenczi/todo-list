-module(todo_SUITE).
-include_lib("common_test/include/ct.hrl").
-export([init_per_suite/1, end_per_suite/1,
         all/0]).
-export([create_todo/1, add_item/1, add_item_not_found/1,
         list_items/1, delete_item/1]).

all() -> [create_todo, add_item, add_item_not_found,
          list_items, delete_item].

init_per_suite(Config) ->
  Priv = ?config(priv_dir, Config),
  application:set_env(mnesia, dir, Priv),
  todo:install([node()]),
  application:start(mnesia),
  application:ensure_all_started(cowboy),
  application:ensure_all_started(prometheus),
  application:ensure_all_started(prometheus_process_collector),
  application:ensure_all_started(prometheus_cowboy),
  application:start(todo),
  Config.
end_per_suite(_Config) ->
  application:stop(mnesia),
  ok.

create_todo(_Config) ->
  ok = todo_srv:create_todo("John Galt", "Monday").

add_item(_Config) ->
  ok = todo_srv:create_todo("John Galt", "Tuesday"),
  ok = todo_srv:add_item("John Galt", "Tuesday", "Build Engine").

add_item_not_found(_Config) ->
  not_found = todo_srv:add_item("John Galt", "Wednesday", "Build Engine").

list_items(_Config) ->
  ok = todo_srv:create_todo("John Galt", "Wednesday"),
  ok = todo_srv:add_item("John Galt", "Wednesday", "Build Engine"),
  ok = todo_srv:add_item("John Galt", "Wednesday", "Fly Airplane"),
  [{todoapp_entry,"John Galt","Wednesday","Build Engine"},{todoapp_entry,"John Galt","Wednesday","Fly Airplane"}] = 
    todo_srv:list_items("John Galt", "Wednesday").

delete_item(_Config) ->
  ok = todo_srv:create_todo("John Galt", "Thursday"),
  ok = todo_srv:add_item("John Galt", "Thursday", "Build Engine"),
  ok = todo_srv:add_item("John Galt", "Thursday", "Fly Airplane"),
  ok = todo_srv:delete_item("John Galt", "Thursday", "Fly Airplane"),
  [{todoapp_entry,"John Galt","Thursday","Build Engine"}] = 
    todo_srv:list_items("John Galt", "Thursday").
