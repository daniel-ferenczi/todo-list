-module(todo).
-behaviour(application).

-record(todoapp_list, {owner,
                       name}).
-record(todoapp_entry, {owner,
                        todo_name,
                        entry_name}).

-export([todoapp_list/2, todoapp_entry/3]).
-export([start/2, stop/1]).
-export([install/1]).

todoapp_list(Owner, Name) ->
  #todoapp_list{owner=Owner, name=Name}.
todoapp_entry(Owner, TodoName, EntryName) ->
  #todoapp_entry{owner=Owner, todo_name=TodoName,
                 entry_name=EntryName}.

start(normal, []) ->
  mnesia:wait_for_tables([todoapp_list, todoapp_entry], 5000),
  todo_sup:start_link().

install(Nodes) ->
  ok = mnesia:create_schema(Nodes),
  rpc:multicall(Nodes, application, start, [mnesia]),
  mnesia:create_table(todoapp_list,
                       [{attributes, record_info(fields, todoapp_list)},
                        {disc_copies, Nodes},
                        {type, bag}]),
  mnesia:create_table(todoapp_entry,
                       [{attributes, record_info(fields, todoapp_entry)},
                        {disc_copies, Nodes},
                        {type, bag}]),
  rpc:multicall(Nodes, application, stop, [mnesia]).

stop(_) ->
  ok.
