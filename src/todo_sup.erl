-module(todo_sup).
-behaviour(supervisor).
-export([start_link/0]).
-export([init/1]).

start_link() ->
  supervisor:start_link(?MODULE, []).

init([]) ->
  PromConfig =
    #{ env => #{ dispatch =>
      cowboy_router:compile(
        [{'_', [{"/metrics/[:registry]", prometheus_cowboy2_handler, []}]}]) }
     },

  Prometheus = ranch:child_spec(
    cowboy_prometheus, 100, ranch_tcp,
    [{port, 9000}],
    cowboy_clear,
    PromConfig),

  Todo = {todosup, {todo_srv, start_link, []}, permanent, 10, worker, dynamic},
  {ok, {{one_for_one, 3, 60}, [Todo, Prometheus]}}.
