-module(todo_srv).
-behaviour(gen_server).

-export([create_todo/2, add_item/3, delete_item/3, list_items/2]).
-export([start_link/0, init/1, handle_call/3, terminate/2]).

start_link() ->
  gen_server:start_link({global, ?MODULE}, ?MODULE, [], [{debug, [trace]}]).

create_todo(Owner, TodoName) ->
  gen_server:call({global, ?MODULE}, {create_todo, {Owner, TodoName}}).

add_item(Owner, TodoName, EntryName) ->
  gen_server:call({global, ?MODULE},
                  {add_item, {Owner, TodoName, EntryName}}).

delete_item(Owner, TodoName, EntryName) ->
  gen_server:call({global, ?MODULE},
                  {delete_item, {Owner, TodoName, EntryName}}).

list_items(Owner, TodoName) ->
  gen_server:call({global, ?MODULE},
                  {list_items, {Owner, TodoName}}).

init([]) ->
  process_flag(trap_exit, true),
  {ok, {}}.

handle_call({create_todo, {Owner, TodoName}}, _From, {}) ->
  TodoList = todo:todoapp_list(Owner, TodoName),
  F = fun() ->
      mnesia:write(TodoList)
  end,
  Result = mnesia:activity(transaction, F),
  {reply, Result, {}};

handle_call({add_item,
            {Owner, TodoName, EntryName}}, _From, {}) ->
  TodoList = todo:todoapp_list(Owner, TodoName),
  TodoEntry = todo:todoapp_entry(Owner, TodoName, EntryName),
  F = fun() ->
          case mnesia:match_object(TodoList) of
            [] -> 
              not_found;
            [_] -> 
              mnesia:write(TodoEntry)
            end
  end,
  Result = mnesia:activity(transaction, F),
  {reply, Result, {}};

handle_call({delete_item, {Owner, TodoName, EntryName}}, _From, {}) ->
  Item = todo:todoapp_entry(Owner, TodoName, EntryName),
  F = fun() ->
          mnesia:delete_object(Item)
  end,
  Result = mnesia:activity(transaction, F),
  {reply, Result, {}};

handle_call({list_items, {Owner, TodoName}}, _From, {}) ->
  Items = todo:todoapp_entry(Owner, TodoName, '_'),
  F = fun() ->
          mnesia:match_object(Items)
  end,
  Result = mnesia:activity(transaction, F),
  {reply, Result, {}}.

terminate(shutdown, _) ->
  ok.
